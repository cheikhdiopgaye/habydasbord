const Sequelize = require('sequelize')
const db = require('../database/db.js')

module.exports = db.sequelize.define(
  'user',
  {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    prenom: {
      type: Sequelize.STRING
    },
    nom: {
      type: Sequelize.STRING
    },
    telephone: {
        type: Sequelize.INTEGER
    },
    adresse: {
        type: Sequelize.STRING
    },
    roles: {
        type: Sequelize.JSON
    },
    email: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
    dateinscrit: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW
    }
  },
  {
    timestamps: false
  }
)