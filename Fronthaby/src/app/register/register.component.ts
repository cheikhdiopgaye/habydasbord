import { Component} from '@angular/core';
import { AuthService, TokenPayload } from '../services/auth.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent  {

  credentials: TokenPayload = {
    id: 0,
    prenom: "",
    nom: "",
    email: "",
    password: "",
    telephone: 0,
    adresse: "",
    roles: ""
  };

  constructor(private auth: AuthService, private router: Router) {}

  register() {
    this.auth.register(this.credentials).subscribe(
      () => {
        this.router.navigateByUrl("/login");
      },
      err => {
        console.error(err);
      }
    );
  }

}
