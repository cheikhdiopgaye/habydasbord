import { Component } from '@angular/core';
import { AuthService, TokenPayload } from '../services/auth.service';
import { Router } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {



  constructor(private auth: AuthService, private router: Router) {}

  credentials: TokenPayload = {
    id: 0,
    prenom: "",
    nom: "",
    email: "",
    password: "",
    telephone: 0,
    adresse: "",
    roles : ""
  }

  login() {
    this.auth.login(this.credentials).subscribe(
      () => {
        this.router.navigateByUrl('/profile');
      },
      err => {
        console.error(err);
      }
    );
  }

}
