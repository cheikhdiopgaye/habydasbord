import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService, TokenPayloadClient } from 'src/app/services/auth.service';

@Component({
  selector: 'app-newclient',
  templateUrl: './newclient.component.html',
  styleUrls: ['./newclient.component.css']
})
export class NewclientComponent  {

  credentials: TokenPayloadClient = {
    id: 0,
    prenom: "",
    nom: "",
    email: "",
    telephone: 0,
    adresse: ""
  };

  constructor(private auth: AuthService, private router: Router) {}

  addClient() {
    this.auth.newClient(this.credentials).subscribe(
      () => {
        console.log('inscription réussie');
        this.router.navigateByUrl("/login");
      },
      err => {
        console.error(err);
        console.log('ya erreur');
      }
    );
  }


}
