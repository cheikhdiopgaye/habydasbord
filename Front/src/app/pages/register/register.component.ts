import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { TokenPayload, AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent  {

  credentials: TokenPayload = {
    id: 0,
    prenom: "",
    nom: "",
    email: "",
    password: "",
    telephone: 0,
    adresse: ""
  };

  constructor(private auth: AuthService, private router: Router) {}

  register() {
    this.auth.register(this.credentials).subscribe(
      () => {
        console.log('inscription réussie');
        this.router.navigateByUrl("/login");
      },
      err => {
        console.error(err);
        console.log('ya erreur');
      }
    );
  }

}
