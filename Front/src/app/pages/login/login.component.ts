import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from "@angular/router";
import { TokenPayload, AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent  {

  credentials: TokenPayload = {
    id: 0,
    prenom: "",
    nom: "",
    email: "",
    password: "",
    telephone: 0,
    adresse: ""
  }
  constructor(private auth: AuthService, private router: Router) {}

  login() {
    this.auth.login(this.credentials).subscribe(
      () => {
        console.log('connection réussi');
        this.router.navigateByUrl('/register');
      },
      err => {
        console.error(err);
      }
    );
  }
}
